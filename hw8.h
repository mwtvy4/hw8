/*
Programmer:  Brandon Valley
Instructor:  Nathan Jarus
Section:     J
Date:        11/2/16
Description: Make a debate
*/

#ifndef HW8_H
#define HW8_H

#include <string>

#include <iostream>
using namespace std;

#include <ctime>
#include <cstdlib>

#include <fstream>
#include <cstring>

//constants
const int WORD_SIZE = 20;// good general size for words
const int SENT_SIZE = 1000;//no sentance over # char's
const int MAX_NUM_LINES = 100;//this ok to use in initialization of all arrays

const char C1_SEN_FILE[] = "candidate1_sentences.dat";
const char C2_SEN_FILE[] = "candidate2_sentences.dat";
const char C1_INT_FILE[] = "candidate1_interjections.dat";
const char C2_INT_FILE[] = "candidate2_interjections.dat";
const char PRE_FILE[] = "prefix_expressions.dat";

//prototypes

//generates a random num between a min and max
//pre: srand initialized, min > max
//post: generates a random num between a min and max
int myRand(const int min, const int max);

//prompts user to input a question to a specific canidate
//pre: canidateNum is initialized
//post: displays the current canidate to be questioned
//      and recieves question from the user and iterates
//      the canidateNum with pass by ref
void promptCanidate(int &canidateNum);

/////////////////////////////
//      fillArrays         //
//VVVVVVVVVVVVVVVVVVVVVVVVV//

//fills array of responces from file and passes the number of
//lines in the file using pass-by-ref
//pre: a[][] must be initialized, numLines must be initialized to 0,  //right way of saying this?????????????????????????
//     file must exist and have data
//post: filles array with data from the file
//      and returns num lines in file using pass-by-ref
void fillArray(char a[][SENT_SIZE], int &numLines, const char FILENAME[]);


//fills array of prefix responces from file and passes the number of
//lines in the file using pass-by-ref
//pre: pre[][] must be initialized, numbLinesPre must be initialized to 0,            //right way of saying this?????????????????????????
//     prefix_expressions file must exist and have data
//post: filles array with prefix expressions from the prefix_expressions file
//      and returns num lines in file using pass-by-ref
void fillArrayPre(char a[][SENT_SIZE], int &numLines);



//^^^^^^^^^^^^^^^^^^^^^^^^^//
//      fillArrays         //
/////////////////////////////

/*
takes in NTCA and scores it using Election-logik©
each character is scored, a RNG determines double and triple letter scores
each word is delimited by anything other than a letter 'a-z'
each word is scored, and a RNG determines double and tripple word scores
at the end of the response (null terminated) the score is tallied up
THERE MUST BE PUNCTUATION OR THE LAST WORD SCORE WILL BE LOST
pre: NTCA with non-letter at the end must be passed in to prevent score loss
RNG must be seeded
post: returns float of response score �
*/


float scoreResponse(const char response[]);


#endif
