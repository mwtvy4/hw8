/*
instructor:  Nathan Jarus
Section:     J
Date:        11/2/16
Description: Make a debate
*/

#include "hw8.h"


//myRand
int myRand(const int min, const int max)
{
  int n = max - min + 1;
  int rand_var;
  rand_var = (rand() % n);
  rand_var += min;

  return rand_var;
}



//prompt canidate
void promptCanidate(int &canidateNum)
{
  static int count = 1;

  char responce[SENT_SIZE];

  //decide if 1 or 2
  if ((count % 2)== 0)//magic num?????????????????????????????????????????????????????????
    canidateNum = 2;//magic num????????????????????????????????????????????????????????
  else
    canidateNum = 1;//magic num???????????????????????????????????????????????????/?/

  cout<<"Canidate #"<<canidateNum<<": ";
  cin>>responce;

  count++;

  return;//test!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}          

/////////////////////////////
//      fillArrays         //
//VVVVVVVVVVVVVVVVVVVVVVVVV//

//fill array
void fillArray(char a[][SENT_SIZE], int &numLines, const char FILENAME[])
{
  ifstream stream;
  stream.open(FILENAME);
  
  char lines[WORD_SIZE];
  stream.getline(lines,WORD_SIZE); //Changed from stream<<numLines 
  numLines = atoi(lines);//cast from char to int

  cout<<numLines<<endl;//test,remove later!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  for(int i = 0; i <= numLines; i++)
  {
    stream.getline(a[i],SENT_SIZE);
	cout<<a[i]<<endl;
  }
  
  stream.close();
  return;
}


/*

  for (int i = 0; i<numLines;i++)
  {
    streamS1.getline(a[i],SENT_SIZE);
    cout<<a[i]<<endl;//test, remove!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  }
  
*/



//fill Prefix array
void fillArrayPre(char a[][SENT_SIZE], int &numLines)
{
  int i = 0;//ok to use i here????????????????????????????????????????????????????????????????????
  
  ifstream streamPre;
  streamPre.open(PRE_FILE);
  
  while(streamPre.getline(a[i],SENT_SIZE))
  {
    cout<<a[i]<<endl;//test, remove!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	numLines++;
	i++;
  }
  
  streamPre.close();
  return;
}
  
//^^^^^^^^^^^^^^^^^^^^^^^^^//
//      fillArrays         //
/////////////////////////////
  
 /* 

//fill arrays
void fillArrays(char pre[][SENT_SIZE])//add char sent1[][Sent_Size]
{
  //declare streams
  //ifstream cInterject1;
 // ifstream cInterject2;
  ifstream cSentance1;
 // ifstream cSentance2;
  ifstream cPrefix;
  
  //open streams
  //cInterject1.open("canidate1_interjections");
  //cInterject2.open("canidate2_interjections");
  cSentance1.open("canidate1_sentances.dat");
  //cSentance2.open("canidate2_sentances.dat");
  cPrefix.open("prefix_expressions.dat");
  
 //declare Vars
  int numLinesPre;
  int lineCount = 0;
  
  //prefix
  while(cPrefix.getline(pre[i],SENT_SIZE))
  {
	
    cout<<pre[i]<<endl;//test, remove!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	i++;
  }
  
  
 
  for(int i = 0; i<3 ;i++)//HOW TO FIX?????????????????????????????????????
  {
    cPrefix.getline(pre[i],SENT_SIZE);
       
    cout<<pre[i]<<endl;//test, remove!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  }
  
  */
 
 
  //sentance 1
//cSentance1.open("canidate1_sentances.dat");
/*
  cSentance1>>numLines;
  
  for (int i = 0; i<numLines;i++)
  {
	cout<<"test"<<endl;  //rREMOVE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    cin.getline(sent[i][SENT_SIZE]);
  }
  
  cSentance1.close();
 
   
 
  // cPrefix.close();
  //cInterject1.close();
  //cInterject2.close();
  //cSentance1.close();
  //cSentance2.close();
 

  
  
  return;
}
  */

float scoreResponse(const char response[])
{
  int i = 0; //index
  int numChars = 0;
  char c; // alias for response[i]
  float baseScore = 0;
  float wordScore = 0;
  float finalScore = 0; 
  int token;//for storing RNG
  while(response[i] != '\0')//loop until the NULL character
  {
    c = tolower(response[i]); 
//    c = response[i];

    if (c=='e'||c=='a'||c=='i'||c=='o'||c=='n'||c=='r'||c=='t'||c=='l'||c=='s'
    ||c=='u')
    {
      baseScore = 1;
      numChars++;
    }

    else if (c == 'd' || c == 'g')
    {
      baseScore = 2;
      numChars++;
    }

    else if (c == 'b' || c == 'c' || c == 'm' || c == 'p')
    {
      baseScore = 3;
      numChars++;
    }

    else if (c == 'f' || c == 'h' || c == 'v' || c == 'w' || c == 'y')
    {
      baseScore = 4;
      numChars++;
    }

    else if (c == 'k')
    {
      baseScore = 5;
      numChars++;
    }

    else if (c == 'j' || c == 'x')
    {
      baseScore = 8;
      numChars++;
    }

    else if (c == 'q' || c == 'z')
    {
      baseScore = 10;
      numChars++;
    }

    else //whitespace, punctuation, etc
         //also, this is a word
    {
      token = rand() % 100;//random number between 0-99
      if (token >=0 && token <=4)// 5% chance
      {
        wordScore *= 2; // double word score
      }
      else if (token == 6 || token == 7)//2% chance
      {
        wordScore *= 3;// triple word score!
      }

      finalScore += wordScore;// add word to final score
      wordScore = 0; //reset word score
      baseScore = 0;//reset baseScore before it passes through letter scores
    }

    token = rand() % 100;// random number between 0-99
    if (token == 1 || token == 2)// 2% chance
    {
      baseScore *= 3; //woohoo!
    }
    else if (token == 3 || token == 4 || token ==5) //3% chance
    {
      baseScore *= 2; //double letter score
    }
    // end character sorting and scoring
    wordScore += baseScore;//add letter to word score
    i++;// increment the counter, move to next character
  }

  //end of response
  if(numChars != 0)//check for 0 denominator
  {
    finalScore = finalScore /static_cast<float>(numChars);//sum of the word scores divided by number of characters
  }
  else
  {
  cout << "You're dividing by zero!" << endl;
  }
  return finalScore;
}














